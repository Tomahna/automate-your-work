import com.typesafe.sbt.packager.docker._

val Http4sVersion = "0.20.12"
val CirceVersion = "0.11.1"
val Specs2Version = "4.1.0"
val LogbackVersion = "1.2.3"

lazy val root = (project in file("."))
  .settings(
    organization := "fr.tomahna",
    name := "automate-your-work",
    scalaVersion := "2.12.8",
    libraryDependencies ++= Seq(
      "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s" %% "http4s-blaze-client" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,
      "io.circe" %% "circe-generic" % CirceVersion,
      "org.specs2" %% "specs2-core" % Specs2Version % "test",
      "ch.qos.logback" % "logback-classic" % LogbackVersion
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.3"),
    addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.0"),
    buildInfoKeys := Seq[BuildInfoKey](name, version),
    buildInfoPackage := "fr.tomahna.automateyourwork",
    dynverSeparator in ThisBuild := "-",
    //docker config
    dockerBaseImage := "openjdk:8-alpine",
    dockerExposedPorts ++= Seq(8080),
    dockerRepository := Some("registry.gitlab.com/tomahna"),
    dockerUpdateLatest := isSnapshot.value,
    //heroku requires that CMD is set and non empty so we set it
    dockerCommands := dockerCommands.value.init,
    dockerCommands += ExecCmd("CMD", (dockerEntrypoint in Docker).value.head)
  )
  .enablePlugins(
    AshScriptPlugin,
    BuildInfoPlugin,
    DockerPlugin,
    JavaServerAppPackaging
  )

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding",
  "UTF-8",
  "-language:higherKinds",
  "-language:postfixOps",
  "-feature",
  "-Ypartial-unification",
  "-Xfatal-warnings"
)

scapegoatVersion in ThisBuild := "1.3.10"
